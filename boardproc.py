# Read in a file of the following format:
# 
# ----------- Start of File ----------
# NxM
# A B ... C
# D E ... F
# .
# .
# .
# G H ... I
# APE
# BAT
# COW
# ------------ End of File -------------
# 
# Where N is the number of rows, M is the number of columns
# followed by N lines of M characters separated by a space
# followed by an arbitrary number of lines each with one word
#
# Returning the dimensions (N,M) as a tuple, board as a List of
# Lists of the characters that follow line 1, and words as a List
# of strings of the words following line N + 1
def read(filename):
    f = open(filename, "r")
    line = f.readline()
    
    # README says separated by a space, but examples have an 'x'
    (n,m) = line.split("x")
    dim = int(n),int(m)

    # read in number of rows into the board equal to the amount specified
    board = []
    for n in range(0,dim[0]):
        board.append(f.readline().split())
    
    # rest of file is a word on each line, so add each to list of words
    words = []
    line = f.readline()
    while line:
        words.append(line.strip())
        line = f.readline()
    
    f.close()

    return dim,board,words


# Write each entry of the solutions tuple List to the output file specified
# in the format:
# 
# WORD A:B X:Y
# 
# Where WORD is the word that the solution is given for, A:B are the start
# x and y coordinates respecively and X:Y are the end x and y coordinates
# respectively of WORD in the board
def write(filename,solutions):
    f = open(filename, "w+")
    print("writing solutions to " + filename + "...")

    for entry in solutions:
        (word,start_x,start_y,end_x,end_y) = entry
        f.write(word + " " + str(start_x) + ":" + str(start_y) + " "
                + str(end_x) + ":" + str(end_y) + "\n")
    
    f.close()