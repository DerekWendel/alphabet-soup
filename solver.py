#!/usr/bin/python
import sys
import boardproc

# given board dimensions dim as a tuple, board as a List of Lists of 
# characters, and words to find as a List of strings, returns a List of 
# tuples in the form (word, start_x, start_y, end_x, end_y)
def solve(dim, board, words):
	# initialize list of solutions
	solutions = []

    # assuming each word only needs to be found once
	for word in words:
		found = False
		i = 0

		# search for start of word until it is found
		while (not found and i < dim[0]):
			j = 0
			while (not found and j < dim[1]):

				# see if word starts at i,j and give location if successful
				found,end = find_solution(dim,board,word,i,j)

				# write solution to the list
				if (found):
					solutions.append((word,i,j,end[0],end[1]))

				j += 1
			
			i += 1

	return solutions

# given board as a List of Lists of characters, word as a string, and i,j as
# coordinates of board, returns a tuple in the form false,None if word does
# not start at i,j in the board and returns true,(end_x,end_y) if word does
# start at i,j and end_x,end_y are the coordinates where word ends
def find_solution(dim,board,word,i,j):
	# make sure first character of word matches this space in board
	if (board[i][j] == word[0]):
		
		length = len(word)
		# iterate through direction(s) the word could be oriented
		for x_dir in range(-1,2):

			for y_dir in range(-1,2):

				# 0,0 is not a valid direction to look for the string
				if not (x_dir == 0 and y_dir == 0):

					# upon finding a match, we already know the direction and
					# length of the word, so return it here
					if (form_string(dim,board,i,j,x_dir,y_dir,length) == word):
						return True,(i + x_dir * (length - 1),
							j + y_dir * (length - 1))
	return False,None

# generates and returns the string when starting at i,j in board, and going 
# length spaces in the direction x_dir,y_dir
def form_string(dim,board,i,j,x_dir,y_dir,length):
	sol_string = ""
	n = 0

	# stop once the length is reached or reached the border of the board
	while (n < length and i + x_dir * n < dim[0] and j + y_dir * n < dim[1]):
		sol_string = sol_string + str(board[i + x_dir * n][j + y_dir * n])
		n += 1

	return sol_string

if __name__ == "__main__":
	
	if len(sys.argv) != 3:
		print("Usage: " + sys.argv[0] + "<input_file> <output_file>")
		sys.exit(1)
	
	# extract information from input file
	dim,board,words = boardproc.read(sys.argv[1])
	
	# get list of words and their locations
	solutions = solve(dim,board,words)

	# write solutions to output
	boardproc.write(sys.argv[2], solutions)
