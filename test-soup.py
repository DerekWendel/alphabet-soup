#!/usr/bin/python
import os
import re
import solver
import boardproc

# Checks structural equality of solution from solver and answer key
# in file filename independent of order (Note: if duplicate words 
# are allowed, number of duplicates in each solution will not be 
# taken into account)
def compare_solution(filename, solution):
    answerkey = []
    f = open(filename, "r")
    line = f.readline()

    # read in answer key
    while line:
        entry = line.split()
        start = entry[1].split(":")
        end = entry[2].split(":")
        answerkey.append((entry[0], int(start[0]), int(start[1]), 
                          int(end[0]), int(end[1])))
        line = f.readline()

    # compare answer key to solution (assuming no duplicate words)
    same = len(answerkey) == len(solution)
    n = 0
    while(same and n < len(answerkey)):
        same = answerkey[n] in solution
        n += 1
 
    return same


for filename in os.listdir("testing/inputs"):
    # filenames with this pattern are numbered tests
    match = re.match("board-(\d+)\.txt", filename)
    if match:
        filenum = match.group(1)
        # extract information from input file
        dim,board,words = boardproc.read("testing/inputs/" + filename)

        # retrieve solutions from solver
        solutions = solver.solve(dim,board,words)

        # compare solution to respective answer key
        for outputfile in os.listdir("testing/outputs"):
            if outputfile == "sol-" + filenum + ".txt":
                result = compare_solution("testing/outputs/" + outputfile,
                                          solutions)
                if result:
                    print("Passed test number " + filenum)
                else:
                    print("Failed test number " + filenum)
                break
